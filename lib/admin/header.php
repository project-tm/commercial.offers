<?php

namespace Commercial\Offers\Admin;

use Commercial\Offers\Config\Iblock,
    Bitrix\Main\Application,
    CIBlockElement,
    CIBlockFormatProperties,
    CSite;

class Header {

    function addHeaders() {
        static $is = false;
        if ($is)
            return;
        $is = true;
        global $APPLICATION;
        $APPLICATION->AddHeadScript('/bitrix/js/main/jquery/jquery-2.1.3.min.min.js');
        \CJSCore::Init(array('window'));
        ob_start();
        ?>
        <script type="text/javascript">
            $(function () {
                $(document).on('click', '.commercial-offers-admin-header span', function () {
                    console.log($(this).closest('table').closest('tr'));
                    $(this).closest('table').closest('tr').prev().find('input').val($(this).text());
                }).on('dblclick', '.commercial-offers-admin-pdf', function () {
                    return false;
                });
            });
        </script>
        <style>
            .commercial-offers-admin-header span {
                font-size: 12px;
                cursor: pointer;
                border-bottom: 1px dashed #000;
                line-height: 20px;
            }
        </style>
        <?

        $APPLICATION->AddHeadString(ob_get_clean());
    }

    function GetUserTypeDescription() {
        return array(
            "USER_TYPE_ID" => "CommercialOffersHeader",
            "CLASS_NAME" => __CLASS__,
            "DESCRIPTION" => "Тип коммерческого предложения",
            "BASE_TYPE" => "int",
        );
    }

    function GetIBlockPropertyDescription() {
        return array(
            "PROPERTY_TYPE" => "S",
            "USER_TYPE" => "CommercialOffersHeader",
            "DESCRIPTION" => "Тип коммерческого предложения",
            'GetPropertyFieldHtml' => array(__CLASS__, 'GetPropertyFieldHtml'),
            'GetAdminListViewHTML' => array(__CLASS__, 'GetAdminListViewHTML'),
        );
    }

    function getViewHTML($name, $value) {
        $request = Application::getInstance()->getContext()->getRequest();
        $ID = $request->get('ID');
        if ($ID) {
            $arSelect = Array("ID", "PROPERTY_NUMBER", 'DATE_CREATE');
            $arFilter = Array("IBLOCK_ID" => Iblock::OFFERS, "ID" => $ID);
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
            if ($arItem = $res->Fetch()) {
                $arItem['DATE'] = CIBlockFormatProperties::DateFormat('d.m.Y', MakeTimeStamp($arItem["DATE_CREATE"], CSite::GetDateFormat()));
                self::addHeaders();
                return '<div class="commercial-offers-admin-header">'
                        . '<span>Коммерческое предложение № ' . $arItem['PROPERTY_NUMBER_VALUE'] . ' от ' . $arItem['DATE'] . '</span><br>
        <span>Счёт от ' . $arItem['DATE'] . '</span><br>
        <span>Накладная от ' . $arItem['DATE'] . '</span></div>';
            }
        } else {
            return '';
        }
    }

    function getEditHTML($name, $value, $is_ajax = false) {
        return self::getViewHTML($name, $value, false);
    }

    function getAdminEditHTML($name, $value, $is_ajax = false) {
        return self::getViewHTML($name, $value, false);
    }

    function GetEditFormHTML($arUserField, $arHtmlControl) {
        return self::getEditHTML($arHtmlControl['NAME'], $arHtmlControl['VALUE'], false);
    }

    function GetAdminListEditHTML($arUserField, $arHtmlControl) {
        return self::getViewHTML($arHtmlControl['NAME'], $arHtmlControl['VALUE'], true);
    }

    function GetAdminListViewHTML($arProperty, $value, $strHTMLControlName) {
        return self::getViewHTML($strHTMLControlName['VALUE'], $value['VALUE']);
    }

    function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName) {
        return self::getAdminEditHTML($strHTMLControlName['VALUE'], $value['VALUE'], false);
    }

}
?>