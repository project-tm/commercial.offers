<?php

namespace Commercial\Offers\Admin;

class Pdf {

    function addHeaders() {
        static $is = false;
        if ($is)
            return;
        $is = true;
        global $APPLICATION;
        $APPLICATION->AddHeadScript('/bitrix/js/main/jquery/jquery-2.1.3.min.min.js');
        \CJSCore::Init(array('window'));
        ob_start();
        ?>
        <script type="text/javascript">
            $(function () {
                var obPopupWin = function (title, content) {
                    console.log(title, content);
                    var item = (new BX.CDialog({
                        width: 500,
                        height: 70,
                        title: title,
                        content: content,
                        name: 'CARDS_UPDATE_MAIL',
                        id: 'CARDS_UPDATE_MAIL',
                        action: function () {
                            this.Close();
                        },
                        onclick: "BX.WindowManager.Get().Close()"
                    }));
        //                    item.SetHead(content);
                    item.Show();
                }
                $(document).on('click', '.commercial-offers-admin-pdf', function () {
                    var ID = 0;
                    if ($(this).closest('tr.adm-list-table-row').is('tr')) {
                        var data = $(this).closest('tr.adm-list-table-row').attr('ondblclick');
                        ID = data.replace(/(^.+&ID=)([0-9]+)(.+)/gi, '$2');
                    } else if ($(this).closest('td.adm-detail-content-cell-r').is('td')) {
                        ID = $(this).closest('form').find('input[name="ID"]').val();
                    }
                    if (ID && confirm("Скачать файл?")) {
                        var a = window.open('/local/modules/commercial.offers/tools/pdf.php?ID='+ID, "_blank", "");
                        a.blur();

//                        console.log(ID);
//                        $.get('/local/modules/commercial.offers/tools/pdf.php', {ID: ID}, function (res) {
//                            if (res.success) {
//                                obPopupWin('Запрос отправлен', res.message);
//                            } else {
//                                obPopupWin('Возникла ошибка', res.error);
//                            }
//                        }, 'json').error(function () {
//                            obPopupWin('Возникла ошибка', 'Ошибка сервера, повторите запрос позднее');
//                        });
                    }
                }).on('dblclick', '.commercial-offers-admin-pdf', function () {
                    return false;
                });
            });
        </script>
        <style>
            .commercial-offers-admin-pdf {
                cursor: pointer;
            }
        </style>
        <?

        $APPLICATION->AddHeadString(ob_get_clean());
    }

    function GetUserTypeDescription() {
        return array(
            "USER_TYPE_ID" => "CommercialOffersPdf",
            "CLASS_NAME" => __CLASS__,
            "DESCRIPTION" => "Коммерческое предложение",
            "BASE_TYPE" => "int",
        );
    }

    function GetIBlockPropertyDescription() {
        return array(
            "PROPERTY_TYPE" => "S",
            "USER_TYPE" => "CommercialOffersPdf",
            "DESCRIPTION" => "Коммерческое предложение",
            'GetPropertyFieldHtml' => array(__CLASS__, 'GetPropertyFieldHtml'),
            'GetAdminListViewHTML' => array(__CLASS__, 'GetAdminListViewHTML'),
        );
    }

    function getViewHTML($name, $value) {
        self::addHeaders();
        return "<div><a class='commercial-offers-admin-pdf'>Скачать</a></div>";
    }

    function getEditHTML($name, $value, $is_ajax = false) {
        return self::getViewHTML($name, $value, false);
    }

    function getAdminEditHTML($name, $value, $is_ajax = false) {
        return self::getViewHTML($name, $value, false);
    }

    function GetEditFormHTML($arUserField, $arHtmlControl) {
        return self::getEditHTML($arHtmlControl['NAME'], $arHtmlControl['VALUE'], false);
    }

    function GetAdminListEditHTML($arUserField, $arHtmlControl) {
        return self::getViewHTML($arHtmlControl['NAME'], $arHtmlControl['VALUE'], true);
    }

    function GetAdminListViewHTML($arProperty, $value, $strHTMLControlName) {
        return self::getViewHTML($strHTMLControlName['VALUE'], $value['VALUE']);
    }

    function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName) {
        return self::getAdminEditHTML($strHTMLControlName['VALUE'], $value['VALUE'], false);
    }

}
?>