<?

use Bitrix\Main\Loader,
    Commercial\Offers\Config\Iblock,
    Commercial\Offers\Utility;

define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);

define('DOMEN_URL', 'http://west-tech.ru');
define('MODUL_URL', DOMEN_URL . '/local/modules/commercial.offers/tools/');

function replace_img_url($content) {
    return str_replace(array('src="/', 'url("/'), array('src="' . DOMEN_URL . '/', 'url("' . DOMEN_URL . '/'), $content);
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

function getContent($code) {
    $content = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/include/pdf/' . $code . '.php');
    $content = str_replace('<?=MODUL_URL ?>', MODUL_URL, $content);
    return $content;
}

$OFFERS_ID = empty($_GET['ID']) ? 0 : (int) $_GET['ID'];
if (Loader::includeModule('commercial.offers') and $OFFERS_ID) {

    $arSelect = Array(
        "ID",
        "NAME",
        'DATE_CREATE',
        'PROPERTY_HEADER',
        'PROPERTY_TYPE',
        'PROPERTY_RESPONSIBLE',
        'PROPERTY_SELLER.PREVIEW_TEXT',
        'PROPERTY_NAME',
        'PROPERTY_EMAIL',
        'PROPERTY_PHONE',
        'PROPERTY_SOURSE',
        'PROPERTY_DATE_DELIVERY',
        'PROPERTY_FREE_DELIVRTY',
        'PROPERTY_TAX',
        'PROPERTY_SIGNATURE',
    );
    $arFilter = Array("IBLOCK_ID" => Iblock::OFFERS, "ID" => $OFFERS_ID);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    if ($arReault = $res->Fetch()) {
        require_once($_SERVER["DOCUMENT_ROOT"] . '/local/vendor/autoload.php');

        define('DEFAULT_FONT', 'tahoma');
        //define('TITLE_FONT', 'robotocondensed');
        define('TITLE_FONT', 'tahoma');
        set_time_limit(1000);
        ini_set('memory_limit', '1024M');
        $html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(15, 10, 35, 10));
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->setDefaultFont('tahoma');


        $html = '<br />';
        $html .= '<table width="680">
		<tr>
		<td width="400">' . getContent('header') . '</td>
		<td width="250" align="right">' . getContent('logo') . '</td>
		</tr>
		</table>';

        $html .= '<table width="680">
		<tr>
			<td style="font-size: 24px; font-weight: bold; text-align: center; width: 680px; padding-top: 15px;">Коммерческое предложение</td>
		</tr>
		<tr>
			<td width="680" style="text-align: center; padding-top: 10px; padding-bottom: 15px;">
			' . htmlspecialchars($arReault['PROPERTY_HEADER_VALUE']) . '<br />
		<strong>Предоставлено в адрес: ' . htmlspecialchars($arReault['PROPERTY_NAME_VALUE']) . '</strong>
		</td></tr></table>';

        $i = 0;
        $total = 0;
        //$has_discount = false;
        $total_discount = 0;
        $html .= '<table width="680" cellpadding="15">';
        $subcats_array = array();
        $counter = 1;

        $arSelect = Array(
            'PROPERTY_PRODUCT_CUSTOM',
            'PROPERTY_PRODUCT_CUSTOM_ARTNUMBER',
            'PROPERTY_PRODUCT_CUSTOM_IMAGE',
            'PROPERTY_PRODUCT',
            'PROPERTY_PRICE',
            'PROPERTY_QUALITY',
            'PROPERTY_DISCOUNT',
            'PROPERTY_DATE',
        );
        $arFilter = Array("IBLOCK_ID" => Iblock::PRODUCT, "PROPERTY_CML2_LINK" => $OFFERS_ID);
        $res = CIBlockElement::GetList(Array('SORT' => 'ASC'), $arFilter, false, false, $arSelect);
        while ($arItem = $res->Fetch()) {

            if ($arItem['PROPERTY_PRODUCT_VALUE'] and empty($arItem['PROPERTY_PRODUCT_CUSTOM'])) {
                $arSelect = Array(
                    'ID',
                    'NAME',
                    'PREVIEW_TEXT',
                    'PREVIEW_PICTURE',
                    'DETAIL_PICTURE',
                    'PROPERTY_ARTNUMBER',
                    'PROPERTY_MANUFACTURER.NAME',
                    'CATALOG_GROUP_1',
                    'DETAIL_PAGE_URL',
                );
                $arFilter = Array("IBLOCK_ID" => Iblock::CATALOG, "ID" => $arItem['PROPERTY_PRODUCT_VALUE']);
                $resProduct = CIBlockElement::GetList(Array('SORT' => 'ASC'), $arFilter, false, false, $arSelect);
                $arProduct = $resProduct->GetNext();
                $image = empty($arProduct['PREVIEW_PICTURE']) ? $arProduct['DETAIL_PICTURE'] : $arProduct['PREVIEW_PICTURE'];
            } else {
                $arProduct = array(
                    'NAME' => $arItem['PROPERTY_PRODUCT_CUSTOM_VALUE'],
                    'PROPERTY_ARTNUMBER' => $arItem['PROPERTY_PRODUCT_CUSTOM_ARTNUMBER_VALUE']
                );
                $image = $arItem['PROPERTY_PRODUCT_CUSTOM_IMAGE_VALUE'];
            }

            if ($image) {
                $image = DOMEN_URL .'/'. CFile::ResizeImageGet($image, array('width' => 100, 'height' => 100), BX_RESIZE_IMAGE_EXACT, true)['src'];
            } else {
                $image = MODUL_URL .'/images/noimage.jpg';
            }

            $price = empty($arItem['PROPERTY_PRICE_VALUE']) ? ($arProduct['CATALOG_PRICE_1'] ?: 0) : (int) $arItem['PROPERTY_PRICE_VALUE'];
            $amounts = empty($arItem['PROPERTY_QUALITY_VALUE']) ? 1 : (int) $arItem['PROPERTY_QUALITY_VALUE'];
            $discounts = empty($arItem['PROPERTY_DISCOUNT_VALUE']) ? 0 : (int) $arItem['PROPERTY_DISCOUNT_VALUE'];

            if ($price) {
                //$price = number_format($price, 2, ',', ' ');
                $priceData = $price;
                $price = number_format($price, 0, '', ' ');
                //$cost	= number_format($price * $amounts, 2, ',', ' ');
                $cost = $priceData * $amounts;
                $costInt = $cost;


                $total += $priceData * $amounts;

                $discount = intval($cost - ($cost / 100 * $discounts));
                if ($discounts) {
                    //$has_discount = true;
                    //$price = '<del>' . $price . ' р.</del><br /><strong>' . ($discount) . ' р.</strong>';
                    $cost = '<del>' . number_format($cost, 0, '', ' ') . ' р.</del><br /><strong>' . (number_format($discount, 0, '', ' ')) . ' р.</strong>';
                } else {
                    //$price = '<strong>' . $price . ' р.</strong>';
                    $cost = '<strong>' . number_format($cost, 0, '', ' ') . ' р.</strong>';
                }

                $total_discount += intval($costInt - ($costInt / 100 * $discounts));
            } else {
                $price = $cost = $discount = 'Звоните';
            }

            $alias = $arProduct ? DOMEN_URL . $arProduct['DETAIL_PAGE_URL'] : DOMEN_URL .'/';
            $html .= '<tr>
				<td width="136" style="border-top: 1px solid #dbdbdb; text-align: center; padding-top: 10px;"><img width="100" src="' . $image . '" /></td>
				<td width="238" style="border-top: 1px solid #dbdbdb; line-height: 22px; padding-top: 10px; padding-left: 10px; padding-right: 5px; padding-bottom: 10px;"><a style="color: #000; display: inline-block; margin-bottom: 10px;" href="' . $alias . '"><strong>' . $arProduct['NAME'] . '</strong></a>
				' . ($arProduct['PROPERTY_ARTNUMBER'] ? '<br />Артикул: ' . htmlspecialchars($arProduct['PROPERTY_ARTNUMBER']) : '') . '
				' . ($arProduct['PREVIEW_TEXT'] ? '<p>' . nl2br(str_replace('P { margin: 0px; }', '', strip_tags(htmlspecialcharsBack($arProduct['PREVIEW_TEXT'])))) . '</p>' : '') . '
				' . ($arProduct['PROPERTY_MANUFACTURER_NAME'] ? '<br />Производитель: ' . htmlspecialchars($arProduct['PROPERTY_MANUFACTURER_NAME']) : '') . '

				</td>
				<td width="190" style="border-top: 1px solid #dbdbdb; font-size: 16px; line-height: 22px; padding-top: 10px;">' . $amounts . ' шт. x ' . $price . ' р.
				' . ($dates[$i] ? '<br /><span style="font-size: 12px;">Дата поставки: ' . htmlspecialchars($dates[$i]) . '</span>' : '') . '
				</td>
				<td width="116" style="border-top: 1px solid #dbdbdb; font-size: 16px; padding-top: 10px;">' . $cost . '</td>
			</tr>';
            $i++;
        }
        $totalInt = $total;
        $total = $total ? '' . number_format($total, 0, '', ' ') . ' р.' : 'Итого: уточняйте у менеджера';
        $nds = round($total_discount - ($total_discount / 1.18), 2);
        $for_nds = $total_discount;

        $html .= '</table>';
        $html .= '
			<table width="680" style="border: 2px solid #feb911; margin-top: 20px;">
				<tr>
                    ' . getContent('footer') . '
					<td width="390" align="right">
					' . ($total_discount < $totalInt ?
                '<span style="font-size: 26px; line-height: 50px;"><span style="line-height: 30px;">Итого: <del>' . $total . '</del><br /><strong>' . number_format($total_discount, 0, '', ' ') . ' р.</strong></span></span>' :
                '<span style="font-size: 26px; font-weight: bold; line-height: 25px;"><br />Итого: ' . $total . '</span>' ) . '
						
					</td>
				</tr>
			</table>
		';

        if ($arReault['PROPERTY_TAX_VALUE']) {
            $html .= '<div>Сумма со скидкой: ' . $for_nds . ' рублей 00 копеек, в т. ч. НДС 18% - ' . $nds . ' руб. 00 коп.</div>';
        }

        $months = array('нулября', 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
        $endDate = strtotime('+7 days');
        $endDate = date('d', $endDate) . ' ' . $months[date('n', $endDate)] . ' ' . date('Y', $endDate);

        $html .= '<br /><br />
    <table width="680px">
	<tr>
		<td width="80px"><img height="60px" src="'.MODUL_URL .'images/offer_excl.jpg" /></td>
		<td width="580px" style="line-height: 20px;">
		<span>' . ($arReault['PROPERTY_FREE_DELIVRTY_VALUE'] == 1 ? 'В стоимость включена доставка.<br />' : '') . '
		' . ($_POST['delivery_date'] ? 'Срок поставки полного заказа ' . htmlspecialchars($arReault['PROPERTY_DATE_DELIVERY_VALUE']) . '.<br />' : '') . '
		Срок действия данного коммерческого предложения до ' . $endDate . ' г.
		</span>
		</td>
	</tr>
	</table>';

        if ($arReault['PROPERTY_SIGNATURE_VALUE']) {
            $html .= '<br /><br /><br /><table>
		<tr>
		<td>Отпустил</td>
		<td>_____________</td>
		<td width="70"></td>

		<td>Получил</td>
		<td>_____________</td>
		</tr>
		<tr>
		<td colspan="4">&nbsp;</td>
		</tr>
		<tr>
		<td>Руководитель</td>
		<td>_____________</td>
		<td></td>

		<td>Бухгалтер</td>
		<td>_____________</td>
		</tr>
		</table>';

            //$p->ln(20);
            //$p->writeHTML($table);
        }

        $html = '<div style="color: #3f3f3f;">' . $html . '</div>';

        $html2pdf->writeHTML($html);
        $html2pdf->Output('West_Tech_' . date('d_m_Y') . '.pdf', 'D');
    }
}