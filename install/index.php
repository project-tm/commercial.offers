<?php

use Bitrix\Main\Application,
    Bitrix\Main\ModuleManager,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader;

IncludeModuleLangFile(__FILE__);

class commercial_offers extends CModule {

    public $MODULE_ID = 'commercial.offers';

    function __construct() {
        $arModuleVersion = array();

        include(__DIR__ . '/version.php');

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_NAME = Loc::getMessage('COMMERCIAL_OFFERS_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('COMMERCIAL_OFFERS_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('COMMERCIAL_OFFERS_PARTNER_NAME');
        $this->PARTNER_URI = '';
    }

    public function DoInstall() {
        ModuleManager::registerModule($this->MODULE_ID);
        $eventManager = Bitrix\Main\EventManager::getInstance();
        $eventManager->registerEventHandler('iblock', 'OnIBlockPropertyBuildList', $this->MODULE_ID, '\Commercial\Offers\Admin\Pdf', 'GetIBlockPropertyDescription');
        $eventManager->registerEventHandler('main', 'OnUserTypeBuildList', $this->MODULE_ID, '\Commercial\Offers\Admin\Pdf', 'GetUserTypeDescription');
        $eventManager->registerEventHandler('iblock', 'OnIBlockPropertyBuildList', $this->MODULE_ID, '\Commercial\Offers\Admin\Header', 'GetIBlockPropertyDescription');
        $eventManager->registerEventHandler('main', 'OnUserTypeBuildList', $this->MODULE_ID, '\Commercial\Offers\Admin\Header', 'GetUserTypeDescription');
        $eventManager->registerEventHandler('iblock', 'OnAfterIBlockElementUpdate', $this->MODULE_ID, '\Commercial\Offers\Event\Element', 'OnAfterIBlockElementUpdate');
        $eventManager->registerEventHandler('iblock', 'OnAfterIBlockElementAdd', $this->MODULE_ID, '\Commercial\Offers\Event\Element', 'OnAfterIBlockElementAdd');
        Loader::includeModule($this->MODULE_ID);
    }

    public function DoUninstall() {
        Loader::includeModule($this->MODULE_ID);
        ModuleManager::unRegisterModule($this->MODULE_ID);
    }

}
